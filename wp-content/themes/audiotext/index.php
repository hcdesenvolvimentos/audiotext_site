<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Audiotext
 */

get_header(); ?>

<div class="pg pg-blog">

	<div class="container">

		<div class="col-sm-8">
			<div class="areaPosts">

				<ul>
					<?php
					
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						$query_args = array('post_type' => 'post','posts_per_page' => 10,'paged' => $paged);
						$the_query = new WP_Query( $query_args );
						$count = $the_query->post_count;
						if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
							global $post;
							$imagemDestacada = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(300,300) );
							$imagemDestacada = $imagemDestacada[0];
							$category_detail=get_the_category($post->ID);//$post->ID
					?>
					<li>
						<figure style="background:url(<?php echo $imagemDestacada ?>)">
							<div class="links">
								<a href="<?php echo $imagemDestacada ?>" id="fancy" rel="gallery1" >
									<i class="fa fa-picture-o" aria-hidden="true"></i>
								</a>	
								<a href="<?php echo get_permalink() ?>">
									<i class="fa fa-external-link" aria-hidden="true"></i>
								</a>
								
							</div>
						</figure>
						<a href="<?php echo get_permalink() ?>">
							<h2><?php echo get_the_title() ?></h2>
							<?php foreach($category_detail as $cd):?>
							<h3><?php echo  $cd->cat_name; ?>,</h3>
							<?php endforeach; ?>
							<p><?php customExcerpt(500); ?></p>

							<span class="link">Ler Mais</span>
						</a>
					</li>
					<?php endwhile; endif; ?>
				</ul>

			</div>
			<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>

		</div>

		<?php get_sidebar(); ?>

	</div>	
	<?php if ($configuracao['paginas_transcricao_seja_um_texter_hidden'] != "Esconder"):?>
	<div class="areaSejaumtexter">
		<p><?php echo $configuracao['opt_inicial_seja_um_texter'] ?></p>
		<a href="<?php echo $configuracao['opt_inicial_seja_um_texter_btn_link'] ?>"><?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?></a>
	</div>
	<?php endif; ?>
</div>
<?php

get_footer();
