<?php
/**
* Template Name: FAQ
* Description: 
*
* @package audiotext
*/
get_header(); ?>
<title><?php echo get_the_title() ?></title>
<div class="pg pg-faq">

	<!-- BANNER -->
	<div class="bannerBackgroud" style="background:url(<?php echo $configuracao['paginas_faq_banner']['url'] ?>)">
		
		<div class="texto">
			<p><?php echo $configuracao['paginas_faq_titulo'] ?></p>
			<div class="info">
				<p><?php echo $configuracao['paginas_faq_descricao'] ?> </p>
			</div>
		</div>
	</div>

	<?php 
		// RECUPERANDO CATEGORIAS
		$categoriaPerguntasFrequentes = array(
			'taxonomy'     => 'categoriaFaq',
			'child_of'     => 0,
			'parent'       => 0,
			'orderby'      => 'name',
			'pad_counts'   => 0,
			'hierarchical' => 1,
			'title_li'     => '',
			'hide_empty'   => 0
		);
		// SE HOUVER CATEGORIAS MOSTRAR ÁREA
		if ($categoriaPerguntasFrequentes):
	
	?>
	<div class="areaConteudo">
		<div class="row">
			<?php 

				$listaCategorias = get_categories($categoriaPerguntasFrequentes);

				// LISTANDO CATEGORIAS
				foreach ($listaCategorias as $listaCategorias):

					// RECUPERANDO PERGUNTAS DE CATEGORIAS				
					$postsPerguntasFrequentes = new WP_Query(array(
						'post_type'     => 'pergunta-frequentes',
						'posts_per_page'   => -1,
						'tax_query'     => array(
							array(
								'taxonomy' => $listaCategorias->taxonomy,
								'field'    => 'slug',
								'terms'    => $listaCategorias->slug,
								)
							)
						)
					);
			?>
			<div class="col-sm-6">
				<!-- ÁREA DE PERGUNTAS -->
				<div class="areadePerguntas">
					<!-- TÍTULO DA CATEGORIA -->
					<h2><?php echo $listaCategorias->name ?></h2>

						<?php 
						//LOOP DE PERGUNTAS  
						while ( $postsPerguntasFrequentes->have_posts() ) : $postsPerguntasFrequentes->the_post();
						global $post;
					?>
					<div class="conteudo">

						<!-- PERGUNTA  -->
						<a class="collapse" data-toggle="collapse" href="#<?php echo $post->ID ?>" aria-expanded="false">
							<h4><?php echo get_the_title() ?></h4>
							<i class="fa fa-plus" aria-hidden="true"></i>
							<i class="fa fa-minus" aria-hidden="true"></i>
						</a>

						<!-- RESPOSTA -->
						<div class="collapse" id="<?php echo $post->ID ?>">
							<div class="card card-block">
								<p>	<?php echo get_the_content() ?></p>
							</div>
						</div>

					</div> 
					<?php   endwhile; wp_reset_query();  ?>

				</div>

			</div>
			<?php endforeach; ?>

		</div>
	</div>
	<?php endif; ?>

	<?php if ($configuracao['paginas_inicial_seja_um_texter_hidden'] != "Esconder"):?>
	<div class="areaSejaumtexter">
		<p><?php echo $configuracao['opt_inicial_seja_um_texter'] ?></p>
		<a href="<?php echo $configuracao['opt_inicial_seja_um_texter_btn_link'] ?>"><?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?></a>
	</div>
	<?php endif; ?>	
</div>
<?php get_footer(); ?>