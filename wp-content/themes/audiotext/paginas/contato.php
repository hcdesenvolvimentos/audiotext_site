<?php
/**
* Template Name: CONTATO
* Description:
*
* @package audiotext
*/
get_header(); ?>
<title><?php echo get_the_title() ?></title>
<!-- PÁGINA DE CONTATO -->
<div class="pg pg-contato">

	<div class="container">
		<div class="row">

			<div class="col-sm-8">
				<div class="formularioOrcamento">
					<!-- ÁREA FOMRULÁRIO -->
					<div class="areaInput">
						<?php echo do_shortcode($configuracao['paginas_contato_formulario']); ?>
					</div>
				</div>
			</div>

			<!-- ÁREA DE CONTATO E ENDEREÇO -->
			<div class="col-sm-4">
				<section class="areaContato">

					<div class="telefones infos">
						<!-- TELEFONE -->
						<div class="titulo">
							<p>Telefone</p>
						</div>

						<!-- TABS DE TELEFONE -->
						<ul class="nav nav-tabs">
							<?php
								// FOREACH PARA PEGAR OS NUMEROS DE CONTATO
								$i = 0;
								if ($configuracao['info_endereco']):
									$telefones_contato = $configuracao['info_endereco'];
									foreach ($telefones_contato as $telefones_contato):
										$telefones_Contato = $telefones_contato;
										$estado = explode(":", $telefones_Contato);
										if ($i == 0):


							?>
							<li class="active" >
								<a data-toggle="tab" href="#tel<?php echo $i ?>"><?php echo $estado[1] ?></a>
							</li>
							<?php else: ?>
							<li>
								<a data-toggle="tab" href="#tel<?php echo $i ?>"><?php echo $estado[1] ?></a>
							</li>
							 <?php endif; $i++;endforeach;endif;?>
						</ul>
						<div class="tab-content">
							<?php
								// FOREACH PARA PEGAR OS NUMEROS DE CONTATO
								$i = 0;
								if ($configuracao['info_endereco']):
									$telefones_contato = $configuracao['info_endereco'];
									foreach ($telefones_contato as $telefones_contato):
										$telefones_Contato = $telefones_contato;
										$estado = explode(":", $telefones_Contato);
										if ($i == 0):


							?>
							<div id="tel<?php echo $i ?>" class="tab-pane fade in active">
								<a itemprop="tel" href="tel:<?php echo $estado[2] ?>"><?php echo $estado[0] ?>: <?php echo $estado[2] ?></a>
							</div>
							<?php else: ?>
							<div id="tel<?php echo $i ?>" class="tab-pane fade">
								<a itemprop="tel" href="tel:<?php echo $estado[2] ?>"><?php echo $estado[0] ?>: <?php echo $estado[2] ?></a>
							</div>
							<?php endif; $i++;endforeach;endif;?>
						</div>
					</div>

					<!-- TAB DE EMAIL -->
					<div class="emails infos">
						<div class="titulo">
							<p>Email</p>
						</div>

						<ul class="nav nav-tabs">
							<?php
								// FOREACH PARA PEGAR OS E-MAILS DE CONTATO
								$i = 0;
								if ($configuracao['info_emial']):
								$info_emialContato = $configuracao['info_emial'];
								foreach ($info_emialContato as $info_emialContato):
									$emailContatoContato = explode(":", $info_emialContato);
									if ($i == 0):
							?>
							<li class="active">
								<a data-toggle="tab" href="#<?php echo $i ?>"><?php echo $emailContatoContato[0] ?></a>
							</li>
							<?php else: ?>
							<li class="">
								<a data-toggle="tab" href="#<?php echo $i ?>"><?php echo $emailContatoContato[0] ?></a>
							</li>
							<?php endif;$i++; endforeach; endif; ?>

						</ul>
						<div class="tab-content">
							<?php
								// FOREACH PARA PEGAR OS E-MAILS DE CONTATO
								$i = 0;
								if ($configuracao['info_emial']):
								$info_emialContato = $configuracao['info_emial'];
								foreach ($info_emialContato as $info_emialContato):
									$emailContatoContato = explode(":", $info_emialContato);
									if ($i == 0):
							?>
							<div id="<?php echo $i ?>" class="tab-pane fade in active">
								<a href="malito:<?php echo $emailContatoContato[1] ?>"><?php echo $emailContatoContato[1] ?></a>
							</div>
							<?php else: ?>
							<div id="<?php echo $i ?>" class="tab-pane">
								<a href="malito:<?php echo $emailContatoContato[1] ?>"><?php echo $emailContatoContato[1] ?></a>
							</div>
							<?php endif;$i++; endforeach; endif; ?>
						</div>
					</div>

					<!-- ENDEREÇOS -->
					<div class="enderecos infos">
						<div class="titulo">
							<p>Endereço</p>
						</div>
						<?php
							//LOOP DE POST ENDEREÇOS
							$postEnderecos = new WP_Query( array( 'post_type' => 'endereco', 'orderby' => 'id', 'posts_per_page' => -1) );
							while ( $postEnderecos->have_posts() ) : $postEnderecos->the_post();
							$Audiotext_endereco_endereco = rwmb_meta('Audiotext_endereco_endereco');

						?>
						<div class="endereco">
							<a href="https://www.google.com.br/maps/place/<?php echo $Audiotext_endereco_endereco = rwmb_meta('Audiotext_endereco_endereco') ?>" target="_blank">
								<h2 itemprop="locality"><?php echo get_the_title() ?></h2>
								<p itemprop="street-address"><?php echo $Audiotext_endereco_endereco = rwmb_meta('Audiotext_endereco_rua') ?></p>
								<span itemprop="region"><?php echo $Audiotext_endereco_endereco = rwmb_meta('Audiotext_endereco_cidade_estado') ?></span>
								<span><?php echo $Audiotext_endereco_endereco = rwmb_meta('Audiotext_endereco_cep') ?></span>
							</a>
						</div>
						<?php endwhile; wp_reset_query(); ?>
					</div>

					<div class="horarios infos">
						<div class="titulo horariosTitulo">
							<p class="horariosTitulo">Horário de Atendimento</p>
						</div>
						<span><?php echo $configuracao['info_horario'] ?></span>
					</div>

				</section>
			</div>
            
		</div>
	</div>

	<!-- SEJA UM TEXTER -->
	<?php if ($configuracao['paginas_inicial_seja_um_texter_hidden'] != "Esconder"):?>
	<div class="areaSejaumtexter">
		<p><?php echo $configuracao['opt_inicial_seja_um_texter'] ?></p>
		<a href="<?php echo $configuracao['opt_inicial_seja_um_texter_btn_link'] ?>"><?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?></a>
	</div>
	<?php endif; ?>
</div>
<?php get_footer(); ?>
