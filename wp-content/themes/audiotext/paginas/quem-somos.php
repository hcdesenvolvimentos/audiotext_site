

<?php
/**
 * Template Name: Quem Somos
 * Description:
 *
 * @package Audiotext
 */

get_header(); ?>
<title><?php  echo get_the_title() ?> </title>
	<div class="pg pg-quemsomos">
		<div class="container">

			<!-- TÍTLE -->
			<div class="title-quemsomos">
				<p><?php echo $configuracao['paginas_quemSomos_titulo'] ?></p>
			</div>
 
			<!-- TEXTO QUEM SOMOS -->
			<div class="texto">
				<p><?php echo $configuracao['paginas_quemSomos_descricao'] ?></p>
			</div>
		</div>
			<div class="listaTexters">
			<!-- PERFIL -->
			<ul>

			<?php

			// DEFINE A TAXONOMIA
			$taxonomia = 'categoriaEquipe';

			// LISTA AS CATEGORIAS PAI DA EQUIPE
			$listarEquipe = get_terms( $taxonomia, array(
				'orderby'    => 'count',
				'hide_empty' => 0,
				'parent'	 => 0
			));

			// PRA CADA SABOR
			foreach ($listarEquipe as $equipe):

			?>

				<?php

					// EXECUTA O LOOP DE ITENS DO CARDÁPIO DA RESPECTIVA SUBCATEGORIA
			    	$integrantes = new WP_Query(
			    	 	array(  
				    		'post_type' => 'equipe',
				    		'posts_per_page' => -1,
				    		'tax_query' => array(
								array(
									'taxonomy' => $taxonomia,
									'field'    => 'slug',
									'terms'    => $equipe->slug,
								)
							)
						)													
					);

					// ENQUANTO HOUVER ITENS NO LOOP
					while ( $integrantes->have_posts() ) : $integrantes->the_post();

					$fotointegrante = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
					$fotointegrante = $fotointegrante[0];
				?>
				<?php

					$segundafotos = rwmb_meta('Audiotext_foto');

					foreach ($segundafotos as $segundafoto ) {
					}

				 ?>

					<li class="foto">
						<!-- FOTO -->
						<div class="foto-perfil" style="background: url(<?php echo $fotointegrante ?>);">
							<div class="foto-perfil-p" style="background: url(<?php echo $segundafoto['url'] ?>);"></div>
						</div>
					
						<!-- NOME -->
						<p class="nome"><?php echo get_the_title();?></p>
						<p class="cargo"><?php echo rwmb_meta('Audiotext_cargo'); ?></p>
						<!-- DESCRIÇÃO -->
						<p class="descricao"><?php echo get_the_content(); ?></p>
					</li>
				<?php
					endwhile;
				?>
			<?php
				endforeach;
			?>
			</ul>
			</div>

	<!-- SEJA UM TEXTER -->
	<?php if ($configuracao['paginas_inicial_seja_um_texter_hidden'] != "Esconder"):?>
	<div class="areaSejaumtexter">
		<p><?php echo $configuracao['opt_inicial_seja_um_texter'] ?></p>
		<a href="<?php echo $configuracao['opt_inicial_seja_um_texter_btn_link'] ?>"><?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?></a>
	</div>
	<?php endif; ?>	
	</div>

	

<?php get_footer(); ?>