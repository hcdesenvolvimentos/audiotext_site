<?php

/**

 * Audiotext functions and definitions

 *

 * @link https://developer.wordpress.org/themes/basics/theme-functions/

 *

 * @package Audiotext

 */



if ( ! function_exists( 'audiotext_setup' ) ) :

/**

 * Sets up theme defaults and registers support for various WordPress features.

 *

 * Note that this function is hooked into the after_setup_theme hook, which

 * runs before the init hook. The init hook is too late for some features, such

 * as indicating support for post thumbnails.

 */

function audiotext_setup() {

	/*

	 * Make theme available for translation.

	 * Translations can be filed in the /languages/ directory.

	 * If you're building a theme based on Audiotext, use a find and replace

	 * to change 'audiotext' to the name of your theme in all the template files.

	 */

	load_theme_textdomain( 'audiotext', get_template_directory() . '/languages' );



	// Add default posts and comments RSS feed links to head.

	add_theme_support( 'automatic-feed-links' );



	/*

	 * Let WordPress manage the document title.

	 * By adding theme support, we declare that this theme does not use a

	 * hard-coded <title> tag in the document head, and expect WordPress to

	 * provide it for us.

	 */

	add_theme_support( 'title-tag' );



	/*

	 * Enable support for Post Thumbnails on posts and pages.

	 *

	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/

	 */

	add_theme_support( 'post-thumbnails' );



	// This theme uses wp_nav_menu() in one location.

	register_nav_menus( array(

		'menu-1' => esc_html__( 'Primary', 'audiotext' ),

	) );



	/*

	 * Switch default core markup for search form, comment form, and comments

	 * to output valid HTML5.

	 */

	add_theme_support( 'html5', array(

		'search-form',

		'comment-form',

		'comment-list',

		'gallery',

		'caption',

	) );



	// Set up the WordPress core custom background feature.

	add_theme_support( 'custom-background', apply_filters( 'audiotext_custom_background_args', array(

		'default-color' => 'ffffff',

		'default-image' => '',

	) ) );



	// Add theme support for selective refresh for widgets.

	add_theme_support( 'customize-selective-refresh-widgets' );

}

endif;

add_action( 'after_setup_theme', 'audiotext_setup' );



/**

 * Set the content width in pixels, based on the theme's design and stylesheet.

 *

 * Priority 0 to make it available to lower priority callbacks.

 *

 * @global int $content_width

 */

function audiotext_content_width() {

	$GLOBALS['content_width'] = apply_filters( 'audiotext_content_width', 640 );

}

add_action( 'after_setup_theme', 'audiotext_content_width', 0 );



/**

 * Register widget area.

 *

 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar

 */

function audiotext_widgets_init() {

	register_sidebar( array(

		'name'          => esc_html__( 'Sidebar', 'audiotext' ),

		'id'            => 'sidebar-1',

		'description'   => esc_html__( 'Add widgets here.', 'audiotext' ),

		'before_widget' => '<section id="%1$s" class="widget %2$s">',

		'after_widget'  => '</section>',

		'before_title'  => '<h2 class="widget-title">',

		'after_title'   => '</h2>',

	) );

}

add_action( 'widgets_init', 'audiotext_widgets_init' );



/**

 * Enqueue scripts and styles.

 */

function audiotext_scripts() {

	

	// FONT

	wp_enqueue_style( 'google-font','https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i');

	

	//JAVA SCRIPT

	wp_enqueue_script( 'audiotext-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js' );

	wp_enqueue_script( 'audiotext-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );

	wp_enqueue_script( 'audiotext-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js' );

	wp_enqueue_script( 'audiotext-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/jquery.fancybox.pack.js' );

	wp_enqueue_script( 'audiotext-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-buttons.js' );

	wp_enqueue_script( 'audiotext-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-media.js' );

	wp_enqueue_script( 'audiotext-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-thumbs.js' );

	wp_enqueue_script( 'audiotext-geral', get_template_directory_uri() . '/js/geral.js' );



	//CSS

	wp_enqueue_style( 'audiotext-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');

	wp_enqueue_style( 'audiotext-bootstrap-font-awesome', get_template_directory_uri() . '/css/font-awesome.css');

	wp_enqueue_style( 'audiotext-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/jquery.fancybox.css');

	wp_enqueue_style( 'audiotext-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-buttons.css');

	wp_enqueue_style( 'audiotext-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-thumbs.css');

	wp_enqueue_style( 'audiotext-owlcarousel', get_template_directory_uri() . '/css/owl.carousel.min.css');

	wp_enqueue_style( 'audiotext-style', get_stylesheet_uri() );



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {

		wp_enqueue_script( 'comment-reply' );

	}

}

add_action( 'wp_enqueue_scripts', 'audiotext_scripts' );



/**

 * Implement the Custom Header feature.

 */

require get_template_directory() . '/inc/custom-header.php';



/**

 * Custom template tags for this theme.

 */

require get_template_directory() . '/inc/template-tags.php';



/**

 * Custom functions that act independently of the theme templates.

 */

require get_template_directory() . '/inc/extras.php';



/**

 * Customizer additions.

 */

require get_template_directory() . '/inc/customizer.php';



/**

 * Load Jetpack compatibility file.

 */

require get_template_directory() . '/inc/jetpack.php';





/********************************************

	REGISTRANDO MENUS

*********************************************/

	function register_my_menus() {

		register_nav_menus(

			array(

				'menuAudioText' => __( 'Menu Topo AudioText' ),

				'menuRodapeAudioText' => __( 'Menu Rodapé AudioText' ),

			)

		);

	}

	add_action( 'init', 'register_my_menus' );

/********************************************

	//FUNÇÃO REDUX//

*********************************************/

if (class_exists('ReduxFramework')) {

	require_once (get_template_directory() . '/redux/sample-config.php');

}



/************************************************************
	//FUNÇÃO PARA MOSTRAR AS IMAGENS DA GALERIA DO REDUX//
************************************************************/
function wp_get_attachment( $attachment_id ) {
    $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}

/***********************************************************
	// VERSIONAMENTO DE FOLHAS DE ESTILO//
************************************************************/
	function versionamentoEstilos($estilos){
		$estilos->default_version = "17082018";
	}
	add_action("wp_default_styles", "versionamentoEstilos");
	// VERSIONAMENTO DE SCRIPTS
	function versionamentoScripts($scripts){
		$scripts->default_version = "17082018";

	}
	add_action("wp_default_scripts", "versionamentoScripts");

/***********************************************************

	// FUNÇÃO EXCERPT CARACTERS //

************************************************************/

function customExcerpt($qtdCaracteres) {

	$excerpt = get_the_excerpt();

	$qtdCaracteres++;

	if ( mb_strlen( $excerpt ) > $qtdCaracteres ) {

		$subex = mb_substr( $excerpt, 0, $qtdCaracteres - 5 );

		$exwords = explode( ' ', $subex );

		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );

		if ( $excut < 0 ) {

			echo mb_substr( $subex, 0, $excut );

		} else {

			echo $subex;

		}

		echo '...';

	} else {

		echo $excerpt;

	}

}

// Organize the uploads in our uploads directory by client_name
function upload_to_client_dir( $folder_path, $form, $field_id, $entry, $feed ) {

    // ID of the field used to collect the client name
    $uploads_field_id = '1';

    // Replace spaces with a hyphen and remove all non-alphanumerics
    $client_dir = str_replace( ' ', '-', rgar( $entry, $uploads_field_id ) );
    $client_dir = preg_replace( '/[^A-Za-z0-9\-]/', '', $client_dir );

    // Create a folder for the client to add their uploads to
    return $folder_path . '/' . $client_dir;

}

// Update the dropbox upload path on a specific form (uploads form)
$uploads_form_arquivo_id = '4';
add_filter( 'gform_dropbox_folder_path_' . $uploads_form_arquivo_id, 'upload_to_client_dir', 10, 5 );




// Organize the uploads in our uploads directory by client_name
function upload_to_client_adicionar( $folder_path, $form, $field_id, $entry, $feed ) {

    // ID of the field used to collect the client name
    $uploads_field_user_id = '1';

    // Replace spaces with a hyphen and remove all non-alphanumerics
    $client_dir_user = str_replace( ' ', '-', rgar( $entry, $uploads_field_user_id ) );
    $client_dir_user = preg_replace( '/[^A-Za-z0-9\-]/', '', $client_dir_user );

    // Create a folder for the client to add their uploads to
    return $folder_path . '/' . $client_dir_user;

}

// Update the dropbox upload path on a specific form (uploads form)
$uploads_form_arquivo_id_user = '5';
add_filter( 'gform_dropbox_folder_path_' . $uploads_form_arquivo_id_user, 'upload_to_client_adicionar', 10, 5 );

//PAGINAÇÃO
function pagination($pages = '', $range = 4){
   $showitems = ($range * 2)+1;
   global $paged;
   if(empty($paged)) $paged = 1;
   if($pages == '')
   {
       global $wp_query;
       $pages = $wp_query->max_num_pages;
       if(!$pages)
       {
           $pages = 1;
       }
   }
   if(1 != $pages)
   {
       echo "<div class=\"paginador\">";
       //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
       //if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
       $htmlPaginas = "";
       for ($i=1; $i <= $pages; $i++)
       {
           if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
           {
               $htmlPaginas .= ($paged == $i)? '<a href="' . get_pagenum_link($i) . '" class="numero selecionado">' . $i . '</a>' : '<a href="' . get_pagenum_link($i) . '" class="numero">' . $i . '</a>';
           }
       }
       if ($paged < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($paged + 1) . '" class="esquerda"><i class="fa fa-chevron-left" aria-hidden="true"></a></i></a>';
       echo $htmlPaginas;
   if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($pages) . '" class="direita"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>';
           echo "</div>\n";
       }
   }
   //REGISTRAR SVG
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');


@ini_set( 'upload_max_size' , '300M' );
@ini_set( 'post_max_size', '300M');
@ini_set( 'max_execution_time', '300' );