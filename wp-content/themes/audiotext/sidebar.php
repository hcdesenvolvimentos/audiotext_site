<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Audiotext
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="col-sm-4">
		
			<div class="sidebar">
				<span>PROCURAR</span>
			
				<div class="areaPesquisa">
					<form method="get" id="searchform" action="<?php echo home_url('/'); ?>" >
					<input type="text" placeholder="Pesquisar" class="field pesquisa" name="s" id="s">
					<input type="submit" name="submit" id="searchsubmit" value="&#xf002;">
					</form>
				</div>

				<span>CATEGORIAS</span>
				<ul class="categoras">
					<?php 
						// CATEGORIA ATUAL
						$categoriaAtual = get_the_category();
						$categoriaAtual = $categoriaAtual[0]->cat_name;
						// LISTA DE CATEGORIAS
						$arrayCategorias = array();
						$categorias = get_categories($args);
						foreach($categorias as $categoria):
							$nomeCategoria = $categoria->name;
							$linkCategoria = get_category_link( $categoria->cat_ID );
					?>
					<li>
						<a href="<?php echo $linkCategoria  ?>"><?php echo $nomeCategoria  ?></a>
					</li>
				<?php endforeach; ?>
					
				</ul>
				<span>TAGS</span>
				
				<div class="tags">
					<?php
						$tags = get_tags();
						foreach ($tags as $tags):
					?>
					<a href="<?php echo $tag_link = get_tag_link($tags->term_id) ?>" data-qtdPost="<?php echo $tags->count ?>" ><?php echo $tags->name ?></a>
					<?php endforeach; ?>
				</div>

				<span>SERVIÇOS DA AUDIOTEXT</span>
				<ul class="categoras">
					<?php 
						// LOOP DE POST VALORES
						$postServicos = new WP_Query( array( 'post_type' => 'servicos', 'orderby' => 'id', 'posts_per_page' => -1) );
						while ( $postServicos->have_posts() ) : $postServicos->the_post();
						if (rwmb_meta('Audiotext_linkUrlServico') != "") {$audiotext_linkUrlServico = rwmb_meta('Audiotext_linkUrlServico');}else{$audiotext_linkUrlServico = get_permalink();}
					?>
					<li>
						<a href="<?php echo $audiotext_linkUrlServico ?>"><?php echo get_the_title() ?></a>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>

			</div>

		</div>
