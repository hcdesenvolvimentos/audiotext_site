<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Audiotext
 */
	global $configuracao;

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NT4GVJJ');</script>
<!-- End Google Tag Manager -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!-- META -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content=""/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" />


	<meta name="author" content="Audiotext">
    <meta name="keywords" content="Audiotext, Audiotext curitiba, transcrição de audio, transcrição de audio para texto online, transcrição de audio para texto em portugues, transcrição de audio preço, transcrição de audio em texto, transcrição de audio para texto preço, transcrição de audio programa, transcrição de audio para texto, transcrição de audio software, transcrição de audio download, degravação de audio para texto, traducao juramentada">

    <!-- <meta name="description" content=""> -->

	<meta property="og:title" content="Audiotext" />
	<meta property="og:description" content="Audiotext Transcrição de Áudio - Transcrever áudio para texto com profissionais especializados. Confira!" />
	<meta property="og:url" content="<?php echo get_site_url() ?>" />
	<meta property="og:image" content="<?php echo get_site_url() ?>/compartilhar.png"/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="Audiotext" />

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="<?php echo get_site_url() ?>" />
	<meta name="twitter:title" content="Audiotext Transcrição de Áudio" />
	<meta name="twitter:description" content="Audiotext Transcrição de Áudio - Transcrever áudio para texto com profissionais especializados. Confira!" />
	<meta name="twitter:image" content="<?php echo get_site_url() ?>/compartilhar.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="canonical" href="<?php echo get_site_url() ?>" />

	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/ico.png">

    <!--  Favicon -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/Ícone-Audiotext.png">

	<!-- TÍTULO -->
	<title>Audiotext - Transcrição de áudio em texto. </title>
	<?php echo $configuracao['scripts_topo_head']  ?>
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67616006-28', 'auto');
  ga('send', 'pageview');

</script>

	<?php wp_head();  ?>
</head>

<body id="body" <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NT4GVJJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<?php echo $configuracao['scripts_topo_body']  ?>
	<header class="topo fixed" id="topo">

		<div class="container containerFull">
			<div class="row">
				<div class="col-md-2">
					<div class="logo">
						<a href="<?php echo home_url('/'); ?>">
							<strong itemprop="name">Audiotext</strong>
						</a>
					</div>
				</div>
				<div class="col-md-10">
					<div class="areaMenu">
						<!-- MENU  -->
						<div class="navbar" role="navigation">

							<!-- MENU MOBILE TRIGGER -->
							<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" ><!--  data-toggle="collapse" data-target="#collapse"-->
								<span class="sr-only"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<img src="<?php echo $configuracao['opt_imagem_fechar_menu_mobile'] ?>" alt="Fechar">
							</button>

							<!--  MENU MOBILE-->
							<div class="row navbar-header">

								<nav class=" navbar-collapse" >
								<div class="menuFullMobile" id="collapse">
									<div class="itensMenu">
									<?php
                                        global $wp;
                                        $current_slug = add_query_arg( array(), $wp->request );
                                        $paginaAtual =  $current_slug;
                                    
                                            if($paginaAtual != 'legendagem-video-aulas-treinamentos-palestras' && $paginaAtual != 'legendagem-filmes-propagandas-videos-youtube' && $paginaAtual !='traducao-sites-softwares-relatorios-apresentacoes' && $paginaAtual !='traducao-juramentada-documentos-pessoal-empresarial' && $paginaAtual != 'degravacao-de-audio-em-texto'&& $paginaAtual !='transcricao-de-audio-em-texto'&& $paginaAtual != '' ){ 
                                    
                                                $menuAudioText = array(
                                                    'theme_location'  => '',
                                                    'menu'            => 'Menu sem âncoras Audiotext',
                                                    'container'       => false,
                                                    'container_class' => '',
                                                    'container_id'    => '',
                                                    'menu_class'      => 'nav navbar-nav',
                                                    'menu_id'         => '',
                                                    'echo'            => true,
                                                    'fallback_cb'     => 'wp_page_menu',
                                                    'before'          => '',
                                                    'after'           => '',
                                                    'link_before'     => '',
                                                    'link_after'      => '',
                                                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                                    'depth'           => 2,
                                                    'walker'          => ''
                                                    );
                                                wp_nav_menu( $menuAudioText );
                                            }else {
                                                $menuAudioText = array(
                                                    'theme_location'  => '',
                                                    'menu'            => 'Menu Topo AudioText',
                                                    'container'       => false,
                                                    'container_class' => '',
                                                    'container_id'    => '',
                                                    'menu_class'      => 'nav navbar-nav',
                                                    'menu_id'         => '',
                                                    'echo'            => true,
                                                    'fallback_cb'     => 'wp_page_menu',
                                                    'before'          => '',
                                                    'after'           => '',
                                                    'link_before'     => '',
                                                    'link_after'      => '',
                                                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                                    'depth'           => 2,
                                                    'walker'          => ''
                                                    );
                                                wp_nav_menu( $menuAudioText );
                                            }
									?>
									</div>
								</div>
								</nav>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<a href="#body" class="ancoraTopo"><img src="<?php bloginfo('template_directory'); ?>/img/backtotop.png"></a>
