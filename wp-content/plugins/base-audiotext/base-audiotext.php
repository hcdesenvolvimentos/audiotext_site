<?php
/**
 * Plugin Name: Base Audiotext
 * Description: Controle base do tema Audiotext.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */
	function baseAudiotext () {
			// TIPOS DE CONTEÚDO
			conteudosAudiotext();
			// TAXONOMIA
			taxonomiaAudiotext();
			// META BOXES
			metaboxesAudiotext();
	}
	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosAudiotext (){
			// TIPOS DE DESTQUE
			tipoDestaque();
			// TIPOS DE VIDEOS DESTAQUE
			tipoVideosDestaque();
			// TIPO  COMO FUNCIONA
			tipoComoFunciona();
			// TIPO  DEPOIMENTOS
			tipoDepoimentos();
			// TIPO  PORQUE CONFIAR NA AUDIOTEXT
			tipovaloresaudiotext();
			// TIPO  PORQUE CONFIAR NA AUDIOTEXT
			tipoServicos();
			// TIPO  FAQ
			tipoFaq();
			// TIPO  EQUIPE
			tipoEquipe();
			// TIPO  AGÊNCIAS
			tipoAgencias();
			// TIPO  AGÊNCIAS
			tipoAgenciasValores();
			// TIPO  ENDEREÇO
			tipoEndereco();
			// TIPO CLIENTES
			tipoClientes();
			// TIPO CLIENTES
			tipoVagas();

			/* ALTERAÇÃO DO TÍTULO PADRÃO */
			add_filter( 'enter_title_here', 'trocarTituloPadrao' );
			function trocarTituloPadrao($titulo){
				switch (get_current_screen()->post_type) {
					case 'equipe':
						$titulo = 'Nome do integrante';
					break;
					case 'clientes' :
						$titulo = 'Nome do cliente';
					break;
					default:
					break;
				}
			    return $titulo;
			}
	}

	/****************************************************
	* CUSTOM POST TYPE
	*****************************************************/
	// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {
			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);
		}

		function tipoVideosDestaque() {
			$rotulosVideosDestaque = array(
									'name'               => 'Videos Destaques',
									'singular_name'      => 'video destaque',
									'menu_name'          => 'Videos Destaques',
									'name_admin_bar'     => 'Videos Destaques',
									'add_new'            => 'Adicionar novo video destaque',
									'add_new_item'       => 'Adicionar novo video destaque',
									'new_item'           => 'Novo video destaque',
									'edit_item'          => 'Editar video destaque',
									'view_item'          => 'Ver video destaque',
									'all_items'          => 'Todos os videos destaques',
									'search_items'       => 'Buscar video destaque',
									'parent_item_colon'  => 'Dos videos destaques',
									'not_found'          => 'Nenhum video destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum video destaque na lixeira.'
								);
			$argsVideosDestaque 	= array(
									'labels'             => $rotulosVideosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-media-video',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'videos-destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('video-destaque', $argsVideosDestaque);
		}

		// CUSTOM POST TYPE AGÊNCIAS PARCEIRAS
		function tipoAgencias() {
			$rotulosAgencias = array(
									'name'               => 'agência',
									'singular_name'      => 'agência',
									'menu_name'          => 'Agências Parceiras',
									'name_admin_bar'     => 'agência',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova agência',
									'new_item'           => 'Nova agência',
									'edit_item'          => 'Editar agência',
									'view_item'          => 'Ver agência',
									'all_items'          => 'Todas as agências',
									'search_items'       => 'Buscar agência',
									'parent_item_colon'  => 'Das agências',
									'not_found'          => 'Nenhuma agência cadastrado.',
									'not_found_in_trash' => 'Nenhuma agência na lixeira.'
								);
			$argsAgencias 	= array(
									'labels'             => $rotulosAgencias,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-businessman',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'agencias' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail')
								);
			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('agencias', $argsAgencias);
		}

		// CUSTOM POST TYPE AGÊNCIAS PARCEIRAS VALORES
		function tipoAgenciasValores() {
			$rotulosAgenciasValores = array(
									'name'               => 'tópico',
									'singular_name'      => 'tópico',
									'menu_name'          => 'Valores Agências',
									'name_admin_bar'     => 'texto',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo tópico',
									'new_item'           => 'Novo tópico',
									'edit_item'          => 'Editar tópico',
									'view_item'          => 'Ver tópico',
									'all_items'          => 'Todos os tópicos',
									'search_items'       => 'Buscar tópico',
									'parent_item_colon'  => 'Dos tópicos',
									'not_found'          => 'Nenhum tópico cadastrado.',
									'not_found_in_trash' => 'Nenhum tópico na lixeira.'
								);
			$argsAgenciasValores 	= array(
									'labels'             => $rotulosAgenciasValores,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-thumbs-up',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'valores-agencias' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);
			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('valoresAgencias', $argsAgenciasValores);
		}

		// CUSTOM POST TYPE COMO FUNCIONA
		function tipoComoFunciona() {
			$rotulosComoFunciona = array(
									'name'               => 'como funciona',
									'singular_name'      => 'como funciona',
									'menu_name'          => 'Como Funciona',
									'name_admin_bar'     => 'como funciona',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo tópico',
									'new_item'           => 'Novo tópico',
									'edit_item'          => 'Editar tópico',
									'view_item'          => 'Ver tópico',
									'all_items'          => 'Todos os tópicos',
									'search_items'       => 'Buscar tópico',
									'parent_item_colon'  => 'Dos tópicos',
									'not_found'          => 'Nenhum tópico cadastrado.',
									'not_found_in_trash' => 'Nenhum tópico na lixeira.'
								);

			$argsComoFunciona 	= array(
									'labels'             => $rotulosComoFunciona,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-admin-settings',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'como-funciona' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array('title')
								);
			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('como-funciona', $argsComoFunciona);
		}

		// CUSTOM POST TYPE CLIENTES
		function tipoClientes() {
			$rotulosClientes = array(
									'name'               => 'Clientes',
									'singular_name'      => 'cliente',
									'menu_name'          => 'Clientes Audiotext',
									'name_admin_bar'     => 'cliente',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo cliente',
									'new_item'           => 'Novo cliente',
									'edit_item'          => 'Editar cliente',
									'view_item'          => 'Ver cliente',
									'all_items'          => 'Todos os clientes',
									'search_items'       => 'Buscar cliente',
									'parent_item_colon'  => 'Dos cliente',
									'not_found'          => 'Nenhum cliente cadastrado.',
									'not_found_in_trash' => 'Nenhum cliente na lixeira.'
								);

			$argsClientes 	= array(
									'labels'             => $rotulosClientes,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-groups',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'clientes' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array('title','thumbnail')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('clientes', $argsClientes);
		}

		// CUSTOM POST TYPE DEPOIMENTOS
		function tipoDepoimentos() {

			$rotulosDepoimentos = array(
									'name'               => 'depoimento',
									'singular_name'      => 'depoimento',
									'menu_name'          => 'Depoimentos',
									'name_admin_bar'     => 'depoimento',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo depoimento',
									'new_item'           => 'Novo depoimento',
									'edit_item'          => 'Editar depoimento',
									'view_item'          => 'Ver depoimento',
									'all_items'          => 'Todos depoimento',
									'search_items'       => 'Buscar depoimento',
									'parent_item_colon'  => 'Dos depoimentos',
									'not_found'          => 'Nenhum depoimento cadastrado.',
									'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
								);

			$argsDepoimentos 	= array(
									'labels'             => $rotulosDepoimentos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-format-status',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'depoimentos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array('title')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('depoimentos', $argsDepoimentos);
		}

		// CUSTOM POST TYPE PORQUE CONFIAR NA AUDIOTEXT
		function tipovaloresaudiotext() {
			$rotulosvaloresaudiotext = array(
									'name'               => 'tópico',
									'singular_name'      => 'tópico',
									'menu_name'          => 'Porque confiar na audiotext ?',
									'name_admin_bar'     => 'tópico',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo tópico',
									'new_item'           => 'Novo tópico',
									'edit_item'          => 'Editar tópico',
									'view_item'          => 'Ver tópico',
									'all_items'          => 'Todos os tópicos',
									'search_items'       => 'Buscar tópico',
									'parent_item_colon'  => 'Dos tópicos',
									'not_found'          => 'Nenhum tópico cadastrado.',
									'not_found_in_trash' => 'Nenhum tópico na lixeira.'
								);

			$argsvaloresaudiotext 	= array(
									'labels'             => $rotulosvaloresaudiotext,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-heart',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'porque-confiar-na-audiotext' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array('title')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('porque-confiar', $argsvaloresaudiotext);
		}

		// CUSTOM POST TYPE SERVIÇOS
		function tipoServicos() {
			$rotulosServicos = array(
									'name'               => 'serviço',
									'singular_name'      => 'serviço',
									'menu_name'          => 'Serviços',
									'name_admin_bar'     => 'serviço',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo serviço',
									'new_item'           => 'Novo serviço',
									'edit_item'          => 'Editar serviço',
									'view_item'          => 'Ver serviço',
									'all_items'          => 'Todos os serviço',
									'search_items'       => 'Buscar serviço',
									'parent_item_colon'  => 'Dos serviço',
									'not_found'          => 'Nenhum serviço cadastrado.',
									'not_found_in_trash' => 'Nenhum serviço na lixeira.'
								);
			$argsServicos 	= array(
									'labels'             => $rotulosServicos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-awards',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'servicos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array('title','editor')
								);
			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('servicos', $argsServicos);
		}

		// CUSTOM POST TYPE DESTAQUES
		function tipoEquipe() {
			$rotulosEquipe = array(
									'name'               => 'Integrantes da equipe',
									'singular_name'      => 'Equipe',
									'menu_name'          => 'Equipe Audiotext',
									'name_admin_bar'     => 'equipe',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo integrante',
									'new_item'           => 'Novo integrante',
									'edit_item'          => 'Editar integrante',
									'view_item'          => 'Ver equipe',
									'all_items'          => 'Toda equipe',
									'search_items'       => 'Buscar integrante',
									'parent_item_colon'  => 'Dos integrante',
									'not_found'          => 'Nenhum integrante cadastrado.',
									'not_found_in_trash' => 'Nenhum integrante na lixeira.'
								);

			$argsEquipe 	= array(
									'labels'             => $rotulosEquipe,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-groups',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'equipe' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('equipe', $argsEquipe);
		}

		// CUSTOM POST TYPE ENDEREÇOS
		function tipoEndereco() {
			$rotulosEndereco = array(
									'name'               => 'endereço',
									'singular_name'      => 'endereço',
									'menu_name'          => 'Endereços Audiotext',
									'name_admin_bar'     => 'endereço',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo endereço',
									'new_item'           => 'Novo endereço',
									'edit_item'          => 'Editar endereço',
									'view_item'          => 'Ver endereço',
									'all_items'          => 'Todos endereços',
									'search_items'       => 'Buscar endereço',
									'parent_item_colon'  => 'Dos endereço',
									'not_found'          => 'Nenhum endereço cadastrado.',
									'not_found_in_trash' => 'Nenhum endereço na lixeira.'
								);
			$argsEndereco 	= array(
									'labels'             => $rotulosEndereco,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-location',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'endereco' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array('title')
								);
			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('endereco', $argsEndereco);
		}

		// CUSTOM POST TYPE FAQ
		function tipoFaq() {
			$rotulosFaq = array(
									'name'               => 'pergunta',
									'singular_name'      => 'pergunta',
									'menu_name'          => 'Perguntas frequentes (FAQ)',
									'name_admin_bar'     => 'pergunta',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova pergunta',
									'new_item'           => 'Nova pergunta',
									'edit_item'          => 'Editar pergunta',
									'view_item'          => 'Ver pergunta',
									'all_items'          => 'Todas pergunta',
									'search_items'       => 'Buscar pergunta',
									'parent_item_colon'  => 'Das pergunta',
									'not_found'          => 'Nenhuma pergunta cadastrado.',
									'not_found_in_trash' => 'Nenhuma pergunta na lixeira.'
								);
			$argsFaq 	= array(
									'labels'             => $rotulosFaq,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'pergunta-frequentes' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array('title','editor')
								);
			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('pergunta-frequentes', $argsFaq);
		}

		// CUSTOM POST TYPE FAQ
		function tipoVagas() {
			$rotulosVagas = array(
									'name'               => 'vaga',
									'singular_name'      => 'vaga',
									'menu_name'          => 'Vagas',
									'name_admin_bar'     => 'vaga',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova vaga',
									'new_item'           => 'Nova vaga',
									'edit_item'          => 'Editar vaga',
									'view_item'          => 'Ver vaga',
									'all_items'          => 'Todas vagas',
									'search_items'       => 'Buscar vagas',
									'parent_item_colon'  => 'Das vagas',
									'not_found'          => 'Nenhuma vaga cadastrado.',
									'not_found_in_trash' => 'Nenhuma vaga na lixeira.'
								);
			$argsVagas 	= array(
									'labels'             => $rotulosVagas,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'vagas' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array('title','editor')
								);
			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('vagas', $argsVagas);
		}

	/****************************************************
	* META BOXES
	*****************************************************/

	function metaboxesAudiotext(){



		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );



	}



		function registraMetaboxes( $metaboxes ){



			$prefix = 'Audiotext_';


			// METABOX VIDEOS DESTAQUE
			$metaboxes[] = array(
				'id'			=> 'videosDestaques',
				'title'			=> 'Detalhes do tópico',
				'pages' 		=> array( 'video-destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Link do vídeo destaque: ',
						'id'    => "{$prefix}link_video_destaque",
						'desc'  => '',
						'type'  => 'text',
					),
				),
			);

			// METABOX VIDEOS DESTAQUE
			$metaboxes[] = array(
				'id'			=> 'detalheVagas',
				'title'			=> 'Detalhes da vaga',
				'pages' 		=> array( 'vagas' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Cargo',
						'id'    => "{$prefix}vaga_cargo",
						'desc'  => '',
						'type'  => 'text',
					),
					array(
						'name'  => 'Cidade',
						'id'    => "{$prefix}vaga_cidade",
						'desc'  => '',
						'type'  => 'text',
					),
					array(
						'name'  => 'Tempo',
						'id'    => "{$prefix}vaga_tempo",
						'desc'  => 'Full Time',
						'type'  => 'text',
					),
				),
			);

			// METABOX COMO FUNCIONA
			$metaboxes[] = array(
				'id'			=> 'detalhescomofunciona',
				'title'			=> 'Detalhes do tópico',
				'pages' 		=> array( 'como-funciona' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Texto: ',
						'id'    => "{$prefix}descricaoComoFunciona",
						'desc'  => '',
						'type'  => 'textarea',
					),
					array(
						'name'  => 'Ícone: ',
						'id'    => "{$prefix}iconeComoFunciona",
						'desc'  => '',
						'type'  => 'image_advanced',
						'max_file_uploads' => 1	,
					),
				),
			);
			// METABOX DEPOIMENTOS
			$metaboxes[] = array(
				'id'			=> 'detalhesdepoimento',
				'title'			=> 'Detalhes do depoimento',
				'pages' 		=> array( 'depoimentos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Logo: ',
						'id'    => "{$prefix}logoComoFunciona",
						'desc'  => '',
						'type'  => 'image_advanced',
						'max_file_uploads' => 1	,
					),
					array(
						'name'  => 'Depoimento: ',
						'id'    => "{$prefix}depoimento",
						'desc'  => '',
						'type'  => 'textarea',
					),
				),
			);
			// METABOX PORQUE CONFIAR NA AUDIOTEXT
			$metaboxes[] = array(
				'id'			=> 'detalhesporqueConfiar',
				'title'			=> 'Detalhes',
				'pages' 		=> array( 'porque-confiar' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Ícone: ',
						'id'    => "{$prefix}iconeConfiar",
						'desc'  => 'Ícones utilizados: http://fontawesome.io/icons/',
						'type'  => 'text',
						'placeholder'  => 'fa fa-address-book-o',
					),
					array(
						'name'  => 'Texto: ',
						'id'    => "{$prefix}textoConfiar",
						'desc'  => '',
						'type'  => 'textarea',
					),
				),
			);
			// METABOX DE SERVIÇOS
			$metaboxes[] = array(
				'id'			=> 'detalhesservicos',
				'title'			=> 'Detalhes do serviço',
				'pages' 		=> array( 'servicos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Ícone: ',
						'id'    => "{$prefix}iconeServico",
						'desc'  => 'Ícones utilizados: http://fontawesome.io/icons/',
						'type'  => 'text',
						'placeholder'  => 'fa fa-address-book-o',
					),
					array(
						'name'  => 'Link: ',
						'id'    => "{$prefix}linkUrlServico",
						'desc'  => '',
						'type'  => 'text',
					),
					array(
						'name'  => 'Itens serviços do menu',
						'id'    => "{$prefix}itemServico_menu",
						'desc'  => '',
						'type'  => 'text',
						'clone' => 'true',
					),
				),
			);
			// METABOX DE EQUEIPE
			$metaboxes[] = array(



				'id'			=> 'detalhesequipeMetabox',

				'title'			=> 'Detalhes do integrante',

				'pages' 		=> array( 'equipe' ),

				'context' 		=> 'normal',

				'priority' 		=> 'high',

				'autosave' 		=> false,

				'fields' 		=> array(



					array(

						'name'  => 'Foto do integrante: ',

						'id'    => "{$prefix}foto",

						'desc'  => '',

						'type'  => 'image_advanced',

						'max_file_uploads' => 1

					),

					array(

						'name'  => 'Cargo: ',

						'id'    => "{$prefix}cargo",

						'desc'  => '',

						'type'  => 'text',



					),





				),



			);
			// METABOX DE ENDEREÇOS
			$metaboxes[] = array(



				'id'			=> 'detalhesenderecoMetabox',

				'title'			=> 'Detalhes do endereço',

				'pages' 		=> array( 'endereco' ),

				'context' 		=> 'normal',

				'priority' 		=> 'high',

				'autosave' 		=> false,

				'fields' 		=> array(



					array(

						'name'  => 'Endereço completo: ',

						'id'    => "{$prefix}endereco_endereco",

						'desc'  => 'Adicione o endereço completo para gerar o link',

						'type'  => 'text',

					),



					array(

						'name'  => 'Cidade/Estado: ',

						'id'    => "{$prefix}endereco_cidade_estado",

						'desc'  => 'Adicione o endereço completo para gerar o link',

						'type'  => 'text',

						'placeholder'  => 'São Paulo/SP',

					),



					array(

						'name'  => 'CEP: ',

						'id'    => "{$prefix}endereco_cep",

						'desc'  => 'Adicione o endereço completo para gerar o link',

						'type'  => 'text',

						'placeholder'  => 'CEP 04578-903',

					),



					array(

						'name'  => 'Rua: ',

						'id'    => "{$prefix}endereco_rua",

						'desc'  => 'Adicione o endereço completo para gerar o link',

						'type'  => 'text',

						'placeholder'  => 'Av. das Nações Unidas, 12551 – 9º e 17º andar',

					),



				),



			);
			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaque',

				'title'			=> 'Detalhes do Destaque',

				'pages' 		=> array( 'destaque' ),

				'context' 		=> 'normal',

				'priority' 		=> 'high',

				'autosave' 		=> false,

				'fields' 		=> array(

					array(

						'name'  => 'Link do destaque: ',

						'id'    => "{$prefix}destaque_link",

						'desc'  => '',

						'type'  => 'text',

					),

					array(

						'name'  => 'Destaque tipo banner: ',

						'id'    => "{$prefix}destaque_verificacao",

						'desc'  => '',

						'type'  => 'checkbox',

					),



				),

			);
			return $metaboxes;
		}
	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaAudiotext () {

		taxonomiaCategoriaEquipe();

		taxonomiaCategoriaComofunciona();

		taxonomiaCategoriaDepoimentos();

		taxonomiaCategoriaFaq();

		taxonomiaCategoriaServicos();

		taxonomiaAgenciasValores();

		taxonomiaVagas();

	}

	function taxonomiaCategoriaComofunciona() {
		$rotulosCategoriaComofunciona = array(
											'name'              => 'Categorias do tópico',
											'singular_name'     => 'Categorias do tópico',
											'search_items'      => 'Buscar categoria do tópico',
											'all_items'         => 'Todas as categorias',
											'parent_item'       => 'Categoria pai',
											'parent_item_colon' => 'Categoria pai:',
											'edit_item'         => 'Editar categoria do tópico',
											'update_item'       => 'Atualizar categoria',
											'add_new_item'      => 'Nova categoria',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias tópicos',
										);

		$argsCategoriaComofunciona 		= array(
											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaComofunciona,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'categoria-como-funciona' ),
										);
		register_taxonomy( 'categoriacomoFunciona', array( 'como-funciona' ), $argsCategoriaComofunciona );
	}

	function taxonomiaCategoriaDepoimentos() {
		$rotulosCategoriaDepoimentos = array(
											'name'              => 'Categorias do depoimento',
											'singular_name'     => 'Categorias do depoimento',
											'search_items'      => 'Buscar categoria do depoimento',
											'all_items'         => 'Todas as categorias',
											'parent_item'       => 'Categoria pai',
											'parent_item_colon' => 'Categoria pai:',
											'edit_item'         => 'Editar categoria do depoimento',

											'update_item'       => 'Atualizar categoria do depoimento',

											'add_new_item'      => 'Nova categoria do depoimento',

											'new_item_name'     => 'Nova categoria do depoimento',

											'menu_name'         => 'Categorias dos depoimento',

										);



		$argsCategoriaDepoimentos 		= array(

											'hierarchical'      => true,

											'labels'            => $rotulosCategoriaDepoimentos,

											'show_ui'           => true,

											'show_admin_column' => true,

											'query_var'         => true,

											'rewrite'           => array( 'slug' => 'categoria-depoimento' ),

										);



		register_taxonomy( 'categoriaDepoimentos', array( 'depoimentos' ), $argsCategoriaDepoimentos );



	}

	function taxonomiaCategoriaEquipe() {



		$rotulosCategoriaEquipe = array(

											'name'              => 'Categorias da equipe',

											'singular_name'     => 'Categorias da equipe',

											'search_items'      => 'Buscar categoria da equipe',

											'all_items'         => 'Todas categorias da equipe',

											'parent_item'       => 'Categoria da equipe pai',

											'parent_item_colon' => 'Categoria da equipe pai:',

											'edit_item'         => 'Editar categoria da equipe',

											'update_item'       => 'Atualizar categoria da equipe',

											'add_new_item'      => 'Nova categoria da equipe',

											'new_item_name'     => 'Nova categoria',

											'menu_name'         => 'Categorias da equipe',

										);



		$argsCategoriaEquipe 		= array(

											'hierarchical'      => true,

											'labels'            => $rotulosCategoriaEquipe,

											'show_ui'           => true,

											'show_admin_column' => true,

											'query_var'         => true,

											'rewrite'           => array( 'slug' => 'categoria-Equipe' ),

										);



		register_taxonomy( 'categoriaEquipe', array( 'equipe' ), $argsCategoriaEquipe );



	}

	function taxonomiaCategoriaFaq() {
		$rotulosCategoriaFaq = array(
											'name'              => 'Categorias de pergunta',
											'singular_name'     => 'Categorias de pergunta',
											'search_items'      => 'Buscar categoria de pergunta',
											'all_items'         => 'Todas as categorias de pergunta',
											'parent_item'       => 'Categoria de pergunta pai',
											'parent_item_colon' => 'Categoria de pergunta pai:',
											'edit_item'         => 'Editar categoria de pergunta',
											'update_item'       => 'Atualizar categoria de pergunta',
											'add_new_item'      => 'Nova categoria de pergunta',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias de pergunta',
										);

		$argsCategoriaFaq 		= array(
											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaFaq,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'categoria-Faq' ),
										);

		register_taxonomy( 'categoriaFaq', array( 'pergunta-frequentes' ), $argsCategoriaFaq );
	}

	function taxonomiaCategoriaServicos() {
		$rotulosCategoriaServicos = array(
											'name'              => 'Categorias de serviços',
											'singular_name'     => 'Categorias de serviço',
											'search_items'      => 'Buscar categoria de serviço',
											'all_items'         => 'Todas as categorias de serviço',
											'parent_item'       => 'Categoria de serviço pai',
											'parent_item_colon' => 'Categoria de serviço pai:',
											'edit_item'         => 'Editar categoria de serviço',
											'update_item'       => 'Atualizar categoria de serviço',
											'add_new_item'      => 'Nova categoria de serviço',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias de serviço',
										);

		$argsCategoriaServicos 		= array(
											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaServicos,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'categoria-Servicos' ),
										);

		register_taxonomy( 'categoriaServicos', array( 'servicos' ), $argsCategoriaServicos );
	}

	function taxonomiaAgenciasValores() {
		$rotulosCategoriaConfiar = array(
											'name'              => 'Categorias de tópicos',
											'singular_name'     => 'Categorias de tópico',
											'search_items'      => 'Buscar categoria de tópico',
											'all_items'         => 'Todas as categorias de tópicos',
											'parent_item'       => 'Categoria de tópico pai',
											'parent_item_colon' => 'Categoria de tópico pai:',
											'edit_item'         => 'Editar categoria de tópico',
											'update_item'       => 'Atualizar categoria de tópico',
											'add_new_item'      => 'Nova categoria de tópico',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias de tópicos',
										);

		$argsCategoriaConfiar 		= array(
											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaConfiar,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'categoria-Confianca' ),
										);

		register_taxonomy( 'categoriaConfiar', array( 'porque-confiar' ), $argsCategoriaConfiar );
	}

	function taxonomiaVagas() {
		$rotulosVagas = array(
											'name'              => 'Categorias de vagas',
											'singular_name'     => 'Categorias de vagas',
											'search_items'      => 'Buscar categoria de vaga',
											'all_items'         => 'Todas as categorias de vagas',
											'parent_item'       => 'Categoria de vaga pai',
											'parent_item_colon' => 'Categoria de vaga pai:',
											'edit_item'         => 'Editar categoria de vaga',
											'update_item'       => 'Atualizar categoria de vaga',
											'add_new_item'      => 'Nova categoria de vaga',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias de vaga',
										);

		$argsCategoriaVagas 		= array(
											'hierarchical'      => true,
											'labels'            => $rotulosVagas,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'categoria-vagas' ),
										);

		register_taxonomy( 'categoriavagas', array( 'vagas' ), $argsCategoriaVagas );
	}
	/****************************************************
	* AÇÕES
	*****************************************************/
	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseAudiotext');
	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {
    	baseAudiotext();
   		flush_rewrite_rules();
	}
	register_activation_hook( __FILE__, 'rewrite_flush' );
